from litestar.contrib.opentelemetry import OpenTelemetryConfig
from litestar.middleware import DefineMiddleware
from opentelemetry import trace
from opentelemetry.exporter.jaeger.thrift import JaegerExporter
from opentelemetry.instrumentation.logging import LoggingInstrumentor
from opentelemetry.propagate import set_global_textmap
from opentelemetry.propagators.jaeger import JaegerPropagator
from opentelemetry.sdk.resources import SERVICE_INSTANCE_ID, SERVICE_NAME, SERVICE_NAMESPACE, SERVICE_VERSION, Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor

from infrastructure.config import Config


def init_tracing(config: Config) -> TracerProvider:
    resource = Resource(
        attributes={
            SERVICE_NAME: config.PROJECT_NAME,
            SERVICE_NAMESPACE: config.TRACING_CONFIG.NAMESPACE,
            SERVICE_INSTANCE_ID: config.TRACING_CONFIG.INSTANCE_ID,
            SERVICE_VERSION: config.TRACING_CONFIG.VERSION,
        }
    )
    tracer = TracerProvider(resource=resource)
    set_global_textmap(JaegerPropagator())
    trace.set_tracer_provider(tracer)
    tracer.add_span_processor(
        BatchSpanProcessor(
            JaegerExporter(
                agent_host_name=config.TRACING_CONFIG.JAEGER_HOST,
                agent_port=config.TRACING_CONFIG.JAEGER_PORT,
            )
        )
    )
    LoggingInstrumentor().instrument()

    return tracer


def init_tracing_for_app(config: Config) -> DefineMiddleware:
    tracer = init_tracing(config=config)
    open_telemetry_config = OpenTelemetryConfig()

    return open_telemetry_config.middleware

