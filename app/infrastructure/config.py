import uuid
from typing import Optional
from typing_extensions import Self
from pydantic import AnyHttpUrl, AnyUrl, Field, model_validator
from pydantic_settings import BaseSettings, SettingsConfigDict

from infrastructure.log import LogFormat, LogLevel


class BaseProjectConfig(BaseSettings):
    model_config = SettingsConfigDict(case_sensitive=True)


class _AsyncPostgresDsn(AnyUrl):
    allowed_schemes = {"postgres", "postgresql", "postgresql+asyncpg"}
    user_required = True


class PostgresConfig(BaseProjectConfig):
    HOST: str = "localhost"
    PORT: int = 5432
    USER: str
    PASSWORD: str
    DB: str
    URI: Optional[_AsyncPostgresDsn] = None

    @model_validator(mode='after')
    def assemble_async_db_connection(self) -> Self:
        self.URI = _AsyncPostgresDsn.build(
            scheme="postgresql+asyncpg",
            username=self.USER,
            password=self.PASSWORD,
            host=self.HOST,
            port=self.PORT,
            path=f"{self.DB or ''}",
        )
        return self

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="POSTGRES_")


class CORSConfig(BaseProjectConfig):
    ORIGINS: list[str] = ["*"]
    METHODS: list[str] | str = ["*"]
    HEADERS: list[str] = ["*"]

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="CORS_")


class TracingConfig(BaseProjectConfig):
    JAEGER_HOST: Optional[str] = None
    JAEGER_PORT: Optional[int] = None
    NAMESPACE: str = "default"
    INSTANCE_ID: str = Field(default=str(uuid.uuid4()))
    VERSION: str = "1.0.0"

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="TRACING_")


class GraylogConfig(BaseProjectConfig):
    HOST: str
    PORT: int

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="GRAYLOG_")


class LogConfig(BaseProjectConfig):
    LEVEL: LogLevel = LogLevel.INFO
    FORMAT: LogFormat = LogFormat.PLAIN
    GRAYLOG: Optional[GraylogConfig] = None

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="LOG_")


class SentryConfig(BaseProjectConfig):
    DSN: Optional[AnyHttpUrl] = None
    STAGE: str

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="SENTRY_")


class KafkaConfig(BaseProjectConfig):
    LOCAL: bool = True
    BOOTSTRAP_SERVERS: str
    SASL_MECHANISMS: str = "SCRAM-SHA-512"
    USER: Optional[str] = None
    PASSWORD: Optional[str] = None
    AUTO_OFFSET_RESET: str = "earliest"
    GROUP_ID: str = "1"
    CA_LOCATION: Optional[str] = None
    SCHEMA_REGISTRY_URL: str = "https://test.com"
    SCHEMA_REGISTRY_USER: Optional[str] = None
    SCHEMA_REGISTRY_PASSWORD: Optional[str] = None

    # topics
    TOPIC_TEST: str

    # schemas
    SCHEMA_TEST_SUBJECT: str
    SCHEMA_TEST_ID: int

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="KAFKA_")


class HealthCheckConfig(BaseProjectConfig):
    PERCENTAGE_MINIMUM_FOR_WORKING_CAPACITY: float = 80.0
    PERCENTAGE_MAXIMUM_FOR_WORKING_CAPACITY: float = 100.0

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="HEALTHCHECK_")


class HTTPServiceConfig(BaseProjectConfig):
    REQUESTS_TIMEOUT: int = 5 * 60  # 5 минут
    ATTEMPTS: int = 5
    BACKOFF_FACTOR: float = 0.1
    STATUSES_FOR_RETRY: list[int] = [401, 408]

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="HTTP_SERVICE_")


class KeycloakConfig(BaseProjectConfig):
    REALM_NAME: str
    URL: AnyHttpUrl
    CLIENT_ID: str
    CLIENT_SECRET: str
    ALGORITHMS: list[str] = ["RS256"]

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="KEYCLOAK_")


class RedisConfig(BaseProjectConfig):
    HOST: str
    PORT: str
    DB: str
    PASSWORD: Optional[str] = None

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="REDIS_")


class Config(BaseProjectConfig):
    PROJECT_NAME: str
    VERSION: str = "0.0.1"
    ENVIRONMENT: str = "dev"
    DEBUG: bool = False

    LOG: LogConfig = LogConfig()
    # CORS_CONFIG: CORSConfig = CORSConfig()
    TRACING_CONFIG: TracingConfig = TracingConfig()
    HEALTHCHECK_CONFIG: HealthCheckConfig = HealthCheckConfig()
    # KEYCLOAK_CONFIG: KeycloakConfig = KeycloakConfig()
    # SENTRY_CONFIG: SentryConfig = SentryConfig()
    # KAFKA_CONFIG: KafkaConfig = KafkaConfig()
    #
    # HTTP_SERVICE_CONFIG: HTTPServiceConfig = HTTPServiceConfig()
    #
    POSTGRES_CONFIG: PostgresConfig = PostgresConfig()
    REDIS_CONFIG: RedisConfig = RedisConfig()


config: Config = Config()
