import sentry_sdk
import structlog


def configure_sentry(dsn: str, environment: str, integration: list = None) -> None:
    logger = structlog.get_logger(__name__)
    if dsn is None:
        raise ValueError("Sentry dsn is empty")

    sentry_sdk.init(
        dsn=dsn,
        environment=environment,
        attach_stacktrace=True,
        request_bodies="always",
        with_locals=True,
        integrations=integration,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        traces_sample_rate=1.0,
    )
    logger.info("Configured sentry")
