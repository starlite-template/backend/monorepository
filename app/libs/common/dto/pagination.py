from common.dto import BaseResult, CustomModel



class PaginationQuery(CustomModel):
    offset: int
    limit: int


class PageInfo(CustomModel):
    page_number: int
    page_size: int


class PaginationResult(BaseResult):
    items: list[BaseResult]
    total_count: int
    page: PageInfo
