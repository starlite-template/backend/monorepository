from humps import camelize
from pydantic import BaseModel


class CustomModel(BaseModel):
    class Config:
        alias_generator = camelize
        populate_by_name = True


class BaseCommand(CustomModel):
    ...


class BaseCommandResult(BaseModel):
    ...


class BaseQuery(CustomModel):
    ...


class BaseResult(CustomModel):
    ...


class BaseUseCaseResult(BaseModel):
    ...
