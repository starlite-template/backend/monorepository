from web.main import create_app

__all__ = [
    "create_app",
]
