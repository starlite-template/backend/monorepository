from dependency_injector import containers
from opentelemetry import trace
import logging
from celery import Celery

from infrastructure.ioc.container import Container
from infrastructure.config import config
from infrastructure.log import configure_logging
from infrastructure.sentry import configure_sentry
from infrastructure.tracing import init_tracing

tracer = trace.get_tracer(__name__)
logger = logging.getLogger(__name__)


def create_app(container: containers.DeclarativeContainer = None) -> Celery:
    container = container or Container()
    init_tracing(config)
    configure_logging(config)
    logger.debug(f"Celery application starting...")

    if hasattr(config, "SENTRY_CONFIG"):
        configure_sentry(config.SENTRY_CONFIG.DSN, config.SENTRY_CONFIG.STAGE)

    logger.debug(f"Celery application configured")
    return Celery('hello', broker='amqp://guest@localhost//')


app = create_app()
