
from common import BaseQuery, BaseResult


class CheckDbQuery(BaseQuery):
    pass


class CheckDbQueryResult(BaseResult):
    status: bool = False
