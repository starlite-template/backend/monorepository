from opentelemetry import trace
import logging

from core.example.dto.check_db import CheckDbQueryResult
from core.example.interfaces.check_db import ICheckDbQueryHandler

tracer = trace.get_tracer(__name__)
logger = logging.getLogger(__name__)


class CheckDbUseCase:
    def __init__(self, handler: ICheckDbQueryHandler):
        self.handler = handler

    async def __call__(self) -> CheckDbQueryResult:
        with tracer.start_as_current_span("CheckDbUseCase.__call__"):
            return await self.handler.ask()
