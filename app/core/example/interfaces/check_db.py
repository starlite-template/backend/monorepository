from abc import ABC, abstractmethod

from common import IQueryHandler
from core.example.dto.check_db import CheckDbQuery, CheckDbQueryResult


class ICheckDbQueryHandler(IQueryHandler, ABC):
    @abstractmethod
    async def ask(self, query: CheckDbQuery = None) -> CheckDbQueryResult:
        raise NotImplementedError
