from typing import Union, Dict, Any
from aiogram.filters import BaseFilter
from aiogram.types import Message


from bot.config import config


class IsAdmin(BaseFilter):
    async def __call__(self, message: Message) -> Union[bool, Dict[str, Any]]:
        return message.from_user.id == config.ADMIN_ID
