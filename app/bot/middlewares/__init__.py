from aiogram import Router

from bot.middlewares.acl import ACLMiddleware
from bot.middlewares.check_input import Checker
from bot.middlewares.item_filter import ItemFilterMiddleware
from bot.middlewares.throttling import ThrottlingMiddleware
from bot.middlewares.wrapper import WrapperMiddleware
from bot.config import Config
from infrastructure.ioc.container import Container


def register_middlewares(container: Container, router: Router, config: Config):
    router.message.middleware(ThrottlingMiddleware(redis=container.gateways.redis))
    router.message.middleware(ACLMiddleware(config.T))
    router.message.middleware(WrapperMiddleware())
    router.message.middleware(Checker())
