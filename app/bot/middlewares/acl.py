from abc import ABC
from typing import Any, Callable, Dict, Awaitable
from aiogram import BaseMiddleware
from aiogram.types import Message


class ACLMiddleware(BaseMiddleware, ABC):
    def __init__(self, admin_username: str) -> None:
        self.admin_username = admin_username
        self.counter = 0

    async def __call__(
        self,
        handler: Callable[[Message, Dict[str, Any]], Awaitable[Any]],
        event: Message,
        data: Dict[str, Any]
    ) -> Any:
        user = data["event_from_user"]
        user_id = user.id

        # user = await User.get(user_id)
        # if user is None:
        #     user = await User.create(user_id=user_id)
        #
        # if user.is_blocked:
        #     await message.answer(
        #         f"Вы заблокированы. По всем вопросам пишите {self.admin_username}"
        #     )
        #     raise CallbackAnswerException()

        data["user"] = user
        return await handler(event, data)
