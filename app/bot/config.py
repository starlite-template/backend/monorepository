from pydantic_settings import SettingsConfigDict
from pydantic import SecretStr

from infrastructure.config import BaseProjectConfig, Config


class PostgresConfig(BaseProjectConfig):
    HOST: str = "localhost"
    PORT: int = 5432
    USER: str
    PASSWORD: str
    DB: str

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="POSTGRES_")


class BotConfig(Config):
    TOKEN: SecretStr
    ADMIN_ID: int
    ADMIN_USERNAME: str

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="TELEGRAM_")

config = BotConfig()
