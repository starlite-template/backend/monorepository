from aiogram.fsm.context import FSMContext


async def set_state_data(state: FSMContext, **state_data) -> None:
    """
    Set data into user's state obj

    :param state:           user's state obj
    :keywords state_data    data that will be set into the state obj

    :returns:               None
    """

    await state.update_data(**state_data)
