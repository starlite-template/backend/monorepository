import asyncio
from aiogram import Bot, Dispatcher, Router
from aiogram.client.session.aiohttp import AiohttpSession
from dependency_injector import containers
from opentelemetry import trace
import logging

from bot.config import config
from bot.models import db
from bot.middlewares import register_middlewares
from bot import filters
from infrastructure.ioc.container import Container
from infrastructure.log import configure_logging
from infrastructure.tracing import init_tracing_for_app

tracer = trace.get_tracer(__name__)
logger = logging.getLogger(__name__)


async def create_app(container: containers.DeclarativeContainer = None) -> None:
    container = container or Container()
    open_telemetry_mw = init_tracing_for_app(config)
    _ = configure_logging(log_level=config.LOG.LEVEL, log_format=config.LOG.FORMAT)
    logger.debug(f"Bot application starting...")

    session = AiohttpSession()
    bot = Bot('42:token', session=session)
    dp = Dispatcher()
    router = Router()

    register_middlewares(container, router, config)
    filters.setup(dp)

    logger.debug(f"Bot application configured")

    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling(bot)


if __name__ == "__main__":
    asyncio.run(create_app())
