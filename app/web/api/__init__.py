from litestar import Router

from .v1 import create_router as create_router_v1

__all__ = ["create_router"]


def create_router() -> Router:
    return Router(
        path="/",
        route_handlers=[create_router_v1()],
    )
