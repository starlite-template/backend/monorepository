from litestar import get
from dependency_injector.wiring import inject, Provide
from typing import Callable
import logging

from core.example.dto.check_db import CheckDbQueryResult
from web.openapi import CustomOperation

logger = logging.getLogger(__name__)


@get("/check_db",
     summary="Check session to db",
     scopes=["use_case"],
     operation_class=CustomOperation)
@inject
async def check_db(use_case: Callable = Provide["use_cases.check_db"]) -> CheckDbQueryResult:
    logger.debug(f"Checking db")
    return await use_case()
