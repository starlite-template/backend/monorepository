from litestar import Router

from . import example

__all__ = ["create_router"]


def create_router() -> Router:
    return Router(
        path="/v1",
        route_handlers=[example.router.check_db],
    )
