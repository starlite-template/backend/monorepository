from dependency_injector import containers
from litestar import Litestar
from litestar.openapi.config import OpenAPIConfig
from litestar.openapi.plugins import ScalarRenderPlugin
from opentelemetry import trace
import logging

from infrastructure.ioc.container import Container
from infrastructure.config import config
from web.log import api_configure_logging
from web.sentry import api_configure_sentry
from web.tracing import api_init_tracing
from web.callbacks import register_callback
from web.handlers import register_error_handlers
# from web.middlewares import register_middleware
from web.healthcheck.router import register_healthcheck

tracer = trace.get_tracer(__name__)
logger = logging.getLogger(__name__)


def create_app(container: containers.DeclarativeContainer = None) -> Litestar:
    container = container or Container()
    open_telemetry_mw = api_init_tracing(config)
    logging_config = api_configure_logging(config)
    logger.debug(f"API application starting...")

    application_config = {
        'debug': config.DEBUG,
        'route_handlers': [],
        'exception_handlers': register_error_handlers(),
        'openapi_config': OpenAPIConfig(
            title=config.PROJECT_NAME,
            version=config.VERSION,
            path="/docs",
            render_plugins=[ScalarRenderPlugin()],
        ),
        'middleware': [open_telemetry_mw],
        'logging_config': logging_config,
    }
    application_config['route_handlers'].append(register_healthcheck(healthcheck_config=config.HEALTHCHECK_CONFIG))
    application_config['route_handlers'].append(register_callback(container=container))
    # application_config['route_handlers'].append(register_middleware(application))

    if hasattr(config, "SENTRY_CONFIG"):
        api_configure_sentry(config.SENTRY_CONFIG.DSN, config.SENTRY_CONFIG.STAGE)

    logger.debug(f"API application configured")
    return Litestar(**application_config)


litestar_app = create_app()
