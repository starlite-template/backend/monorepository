from litestar.logging import LoggingConfig

from infrastructure.config import Config
from infrastructure.log import configure_logging


def api_configure_logging(config: Config) -> LoggingConfig:
    log_config = configure_logging(config)

    return LoggingConfig(
        root={"level": config.LOG.LEVEL.value, "handlers": ["graylog" if config.LOG.GRAYLOG else "default"]},
        loggers=log_config['loggers'],
        handlers=log_config['handlers'],
        formatters=log_config['formatters'],
    )
