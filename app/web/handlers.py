from enum import Enum
from itertools import starmap
from typing import Any
from litestar import MediaType, Request, Response
from litestar.status_codes import (
    HTTP_502_BAD_GATEWAY,
    HTTP_408_REQUEST_TIMEOUT,
    HTTP_422_UNPROCESSABLE_ENTITY,
    HTTP_500_INTERNAL_SERVER_ERROR,
)
from litestar.exceptions import ValidationException
import structlog
from humps import camelize
from opentelemetry import trace
from pydantic import ValidationError

from common.errors import BaseBusinessError, ErrorCategories, UnexpectedBusinessError, ValidationBusinessError
from web.errors import HTTPCustomError

logger = structlog.getLogger(__name__)
tracer = trace.get_tracer(__name__)


def _camelize_dict_factory(result: list[tuple[str, Any]]) -> dict:
    with tracer.start_as_current_span("_camelize_dict_factory"):
        return dict(
            starmap(lambda key, value: (camelize(key), value.value if isinstance(value, Enum) else value), result)
        )


def _as_camelized_dict(exc: BaseBusinessError) -> dict:
    with tracer.start_as_current_span("_as_camelized_dict"):
        return dict(_camelize_dict_factory(exc.as_dict().items()))


def http_validation_business_error_handler(_: Request, exc: ValidationBusinessError) -> Response:
    status_code = getattr(exc, "status_code", HTTP_408_REQUEST_TIMEOUT)
    detail = getattr(exc, "detail", _as_camelized_dict(exc))
    with tracer.start_as_current_span("http_business_error_handler"):
        logger.info(exc)

        return Response(
            media_type=MediaType.TEXT,
            content=detail,
            status_code=status_code,
        )


def http_business_error_handler(_: Request, exc: HTTPCustomError) -> Response:
    status_code = getattr(exc, "status_code", HTTP_502_BAD_GATEWAY)
    detail = getattr(exc, "detail", _as_camelized_dict(exc.business_error))
    with tracer.start_as_current_span("http_business_error_handler"):
        logger.info(exc)

        return Response(
            media_type=MediaType.TEXT,
            content=detail,
            status_code=status_code,
        )


def http_validation_error_handler(_: Request, exc: ValidationError) -> Response:
    status_code = getattr(exc, "status_code", HTTP_422_UNPROCESSABLE_ENTITY)
    with tracer.start_as_current_span("http_validation_error_handler"):
        logger.info(exc)

        return Response(
            media_type=MediaType.TEXT,
            content=_as_camelized_dict(
                ValidationBusinessError(
                    status="422",
                    detail=str(exc),
                    data={"errors": exc.errors()},
                    raw_type=exc.__class__.__name__,
                    category=ErrorCategories.VALIDATION_EXCEPTION,
                )
            ),
            status_code=status_code,
        )


def http_internal_server_error_handler(_: Request, exc: Exception) -> Response:
    status_code = getattr(exc, "status_code", HTTP_500_INTERNAL_SERVER_ERROR)
    with tracer.start_as_current_span("http_internal_server_error_handler"):
        logger.error(exc)

        return Response(
            media_type=MediaType.TEXT,
            content=_as_camelized_dict(
                UnexpectedBusinessError(
                    status="500",
                    detail=str(exc),
                    raw_type=exc.__class__.__name__,
                    category=ErrorCategories.UNEXPECTED_EXCEPTION,
                )
            ),
            status_code=status_code,
        )


def register_error_handlers() -> {}:
    with tracer.start_as_current_span("_register_error_handlers"):
        return {
            ValidationBusinessError: http_validation_business_error_handler,
            HTTPCustomError: http_business_error_handler,
            ValidationException: http_validation_error_handler,
            ValidationError: http_validation_error_handler,
            Exception: http_internal_server_error_handler,
        }
