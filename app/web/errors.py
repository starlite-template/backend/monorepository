from typing import Any, Optional
from litestar.exceptions import HTTPException

from common.errors import BaseBusinessError



class HTTPCustomError(HTTPException):
    __slots__ = (
        "status_code",
        "detail",
        "headers",
        "extra",
        "_business_error",
    )

    def __init__(
        self,
        status_code: int,
        business_error: BaseBusinessError,
        detail: Optional[Any] = None,
        headers: Optional[dict[str, Any]] = None,
        extra: dict[str, Any] | list[Any] | None = None,
    ) -> None:
        super().__init__(status_code, detail, headers, extra)
        self._business_error = business_error

    @property
    def business_error(self) -> BaseBusinessError:
        return self._business_error
