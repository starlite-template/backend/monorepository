import sentry_sdk
import structlog
from litestar_sentry import LitestarIntegration

from infrastructure.sentry import configure_sentry


def api_configure_sentry(dsn: str, environment: str):
    configure_sentry(dsn, environment, integration=[
        LitestarIntegration(),
    ])
