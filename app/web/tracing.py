from litestar.contrib.opentelemetry import OpenTelemetryConfig
from litestar.middleware import DefineMiddleware

from infrastructure.config import Config
from infrastructure.tracing import init_tracing


def api_init_tracing(config: Config) -> DefineMiddleware:
    tracer = init_tracing(config=config)
    open_telemetry_config = OpenTelemetryConfig()

    return open_telemetry_config.middleware
