from opentelemetry import trace

from infrastructure.ioc.container import Container
from web.api import create_router

tracer = trace.get_tracer(__name__)


def register_callback(container: Container) -> []:
    with tracer.start_as_current_span("register_callback"):
        router = create_router()
        # router.register(openapi)
        return router
