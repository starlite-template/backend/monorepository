from abc import ABC, abstractmethod
from typing import Any
from dependency_injector.wiring import Closing, Provide, inject
from pydantic import BaseModel, Field
from sqlalchemy import select, text
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession
from utils.time import TimeCatcher
import logging

from web.healthcheck.status import Status

logger = logging.getLogger(__name__)


class CommandResult(BaseModel):
    status: Status
    data: dict = Field(default_factory=dict)
    duration: float
    exception: str | None = None
    description: str | None = None


class BaseCommand(ABC):
    @abstractmethod
    async def execute(self) -> CommandResult:
        """
        Запуск проверки сервиса
        :return:
        """
        ...


class DBCommand(BaseCommand):
    @inject
    async def execute(self, db_session: AsyncSession = Closing[Provide["gateways.db_session"]]) -> CommandResult:
        async with TimeCatcher() as catcher:
            try:
                await db_session.execute(select(text("1")))
                status, exception, description = Status.HEALTHY, None, None
            except SQLAlchemyError as e:
                status, exception, description = Status.UNHEALTHY, str(type(e)), str(e)

        return CommandResult(
            status=status,
            duration=catcher.total_duration,
            exception=exception,
            description=description,
        )


class OpenSearchPingCommand(BaseCommand):
    def __init__(self, client: Any = Closing[Provide["gateways.open_search_client"]]):
        self.client = client

    async def execute(self) -> CommandResult:
        async with TimeCatcher() as catcher:
            try:
                client = await self.client
                await client.info()
                status, exception, description = Status.HEALTHY, None, None
            except Exception as e:
                status, exception, description = Status.UNHEALTHY, str(type(e)), str(e)

        return CommandResult(
            status=status,
            duration=catcher.total_duration,
            exception=exception,
            description=description,
        )
