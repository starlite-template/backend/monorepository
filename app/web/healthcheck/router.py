from litestar import Router, get
from litestar.di import Provide
from litestar.status_codes import HTTP_200_OK, HTTP_400_BAD_REQUEST
from litestar.exceptions import HTTPException
from opentelemetry import trace
import logging

from infrastructure.config import HealthCheckConfig
from web.healthcheck.commander import HealthCheckCommander, WorkingCapacityResult
from web.healthcheck.handler import CommandHandler
from web.healthcheck.status import Status
from web.healthcheck.commands import DBCommand

tracer = trace.get_tracer(__name__)
logger = logging.getLogger(__name__)


def register_healthcheck(healthcheck_config: HealthCheckConfig) -> Router:
    with tracer.start_as_current_span("register_healthcheck"):

        liveness_command_handler = CommandHandler()
        readiness_command_handler = CommandHandler()
        monitor_command_handler = CommandHandler()

        # TODO Need to implement your Command classes. Example:
        # liveness_command_handler.add_healthcheck_command(service_name="Database", command=DBCommand())
        # readiness_command_handler.add_healthcheck_command(service_name="Database", command=DBCommand)
        # monitor_command_handler.add_healthcheck_command(command=kafka_command)

        readiness_command_handler.add_healthcheck_command(service_name="Database", command=DBCommand)


        healthcheck_commander = HealthCheckCommander(
            liveness_handler=liveness_command_handler,
            readiness_handler=readiness_command_handler,
            monitor_handler=monitor_command_handler,
            healthcheck_config=healthcheck_config,
        )

        @get(
            "/liveness",
            dependencies={
                "result": Provide(
                    healthcheck_commander.run_liveness_route,
                )
            },
            status_code=HTTP_200_OK,
            include_in_schema=True,
        )
        async def liveness(result: WorkingCapacityResult) -> WorkingCapacityResult:
            with tracer.start_as_current_span("liveness"):
                if result.status == Status.HEALTHY:
                    return result
                else:
                    raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail=result)

        @get(
            "/readiness",
            dependencies={
                "result": Provide(
                    healthcheck_commander.run_readiness_route,
                )
            },
            status_code=HTTP_200_OK,
            include_in_schema=True,
        )
        async def readiness(result: WorkingCapacityResult) -> WorkingCapacityResult:
            with tracer.start_as_current_span("readiness"):
                if result.status == Status.HEALTHY:
                    return result
                else:
                    raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail=result)

        @get(
            "/monitor",
            dependencies={
                "result": Provide(
                    healthcheck_commander.run_monitor_route,
                )
            },
            status_code=HTTP_200_OK
        )
        async def monitor(result: WorkingCapacityResult) -> WorkingCapacityResult:
            with tracer.start_as_current_span("monitor"):
                if result.status == Status.HEALTHY:
                    return result
                else:
                    raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail=result)

        return Router(path="/health", route_handlers=[
            liveness,
            readiness,
            monitor
        ])
