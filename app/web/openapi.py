from dataclasses import dataclass, field
from dependency_injector.wiring import Provide
from copy import deepcopy

from litestar.openapi.spec import Operation


@dataclass
class CustomOperation(Operation):
    """Custom Operation class which includes a non-standard field which is part of an OpenAPI extension."""

    x_code_samples: list[dict[str, str]] | None = field(default=None, metadata={"alias": "x-codeSamples"})

    def __post_init__(self) -> None:
        parameters = deepcopy(self.parameters)
        for parameter in parameters:
            if isinstance(parameter.schema.default, Provide):
                parameters.remove(parameter)
                continue
        self.parameters = parameters
